# Biological network data tutorial


* **Author:** Susan Steinbusch-Coort

* **Last updated** 2024-09-20

---

### In this skills session, you will learn:
-	how to upload a biological network from STRING 
-	how to extend a network using CyTargetLinker
-	how to load a pathway diagram from WikiPathways
-	how to upload a network shared via NDEx 
-	how to visualize data in a network
-	how to create a sub network
-	how to extend a network via R

---

### Preparation 

* For the assignments you need to download and unpack the data for the two use cases
* Download UseCase1.zip from [here](https://surfdrive.surf.nl/files/index.php/s/BiMSkuLP40CFcbl)
* Download UseCase2.zip from [here](https://surfdrive.surf.nl/files/index.php/s/edsNITgoTCA6Ld1)
* Unpack both files
* In UseCase1 also unpack in directory LinkSets -> chembl_23_hsa_20180126.zip


Op Cytoscape and go to Apps -> App Store -> Show App Store and install the following Cytoscape apps: 
1. stringApp
2. WikiPathways
3. CyTargetLinker 
4. CyNDEx-2


Restart Cytoscape after installing the Apps. 

---

### Go through the following parts and answer the questions

- Part A: [STRING](http://coort.gitlab.io/tutorial-network-data/pages/string.html)
- Part B: [WikiPathways](http://coort.gitlab.io/tutorial-network-data/pages/WikiPathways.html)
- Part C: [NDEx](http://coort.gitlab.io/tutorial-network-data/pages/NDEx.html)
- Part D: [Automation](http://coort.gitlab.io/tutorial-network-data/pages/Automation.html)
- Part E: [Application](http://coort.gitlab.io/tutorial-network-data/pages/Application.html)

