# Import and extend a Rett Syndrome PPI network from STRING in Cytoscape

Rett syndrome is a rare neurological disease caused by a mutation in the methyl-CpG-binding protein 2 (MECP2) gene. 
First we will use the stringApp to create a protein-protein interaction (PPI) network for the Rett syndrome (Disease Query)

* Select the “STRING disease query”

![alt text](img/img_string.png "STRING disease query")

* Set the criteria by selecting “more options” to a confidence (score) cut-off = 0.4 and maximal number of proteins = 20

![alt text](img/img_criteria.png "Set criteria")

* Search for the “Rett syndrome”.

##### The PPI network linked to the Rett Syndrome is now generated in Cytoscape.

* On the right-hand side of the PPI network a legend is given. In this legend you can change the style of the network, apply filters for tissues and cellular compartments. To get more information about a protein you can select a node and under "selected nodes"; crosslinks, a description and the structure is displayed.  

> Question 1: Investigation of the STRING Rett syndrome network
> - How many nodes are in the network and are they all connected? 
> - How many edges are in the network?
> - Which types of interaction evidence are present in the PPI network?
> - Which PPI has the highest score? 
> - Which additional information related to the proteins is given?  

Now you are going to extend the PPI Rett syndrome network, using CyTargetLinker with compound-target interactions from [ChEMBL](https://www.ebi.ac.uk/chembl/) and disease-gene associations from a manually curated subset for rare diseases from [OMIM](https://omim.org/). ChEMBL is an open online bioactivity database containing information about compounds, their bioactivity and their possible targets (including proteins). OMIM is a comprehensive collection of human genetic phenotypes and their associated human genes. 

* Go to Apps -> CyTargetLinker -> Exend network
    * Select User Network
        * choose the network name of the STRING network
    * Select your network attribute	
        * choose the name of the column containing a biological identifier (“stringdb::canonical name”)
    * Select Link Sets
        * Browse in the UseCase1 files for the directory which contains the Link Sets 
    * Select direction 
        * “SOURCES”
* Click at Ok

![alt text](img/cytargetlinker1.PNG "Set CyTargetlinker criteria")

* Select the ChEMBL (chembl_23_hsa_20180126.xgmml) and the OMIM (rare-disease-gene-associations-has-20180411.xgmml) LinkSets
* Click at OK

![alt text](img/cytargetlinker2.PNG "Select LinkSets")

##### Change the node labels in network
* Click at the "Style" panel at the left hand side
* Go to label and click at the icon in the Map. column
* Select "display name" as the correct column 

![alt text](img/Display.png "Select label")


##### Open the CyTargetLinker results panel.
* Go to Apps -> CyTargetLinker-> Show Results Panel. 
* At the right hand side the panel is opened and select the Extended network (CTL_String Network-Rett Syndrome).
* Now the amount of added interactions per Link Set is shown.

> Question 2: Investigation of extended STRING Rett network
> - How many nodes are in the network and are they all connected? 
> - How many edges are in the network?
> - How many interactions are added from ChEMBL and how many from OMIM? 
> - What type of information does ChEMBL and OMIM add to the network? 

