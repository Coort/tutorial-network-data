# Connecting R to Cytoscape for automation

The downloaded UseCase1.zip file contained and Rmd script. Walk through the **UseCase1.Rmd** script – one part at a time! You will extend the STRING Rett syndrome network as we did before but it has a different setting while extending. 
If you want to explore the various functionalities of the CyTargetLinker app go to View -> Show Automation Panel and type help cytargetlinker. Now you will see all the functions. Per function you can set parameters. 

[Paper:](https://f1000research.com/articles/7-743/v2) Kutmon M, Ehrhart F, Willighagen EL, Evelo CT and Coort SL. CyTargetLinker app update: A flexible solution for network extension in Cytoscape (2019) F1000Research

The Rmd-file of UseCase 2 from the CyTargetLinker automation paper is also provided in de downloaded UseCase2.zip (**UseCase2.Rmd**). Walk through the file and discover the R-code. This UseCase starts with loading the a list of differentially expressed genes in the Purkinje cells located in the cerebellar cortex of the brain of a Mecp2−/y mouse model for Rett syndrome.

Finally, we have a [UseCase 3](https://surfdrive.surf.nl/files/index.php/s/Ektq1SdJDHcl2SU), also described in the automation paper, in which author nodes are extended with their publications and journal information. You can download UseCase3 [here](https://gitlab.com/coort/tutorial-network-data/raw/master/data/UseCase3.zip?inline=false)

* Feel free to use some time to investigate your two selected interaction resources from Case 2. 
* The tutorials and more information about CyTargetLinker can be found [here](https://cytargetlinker.github.io/)
