# Extend the MECP2 pathway in WikiPathways with other processes

In this part, we are going to open a pathway from [WikiPathways](https://www.wikipathways.org/index.php/WikiPathways) as a network and extend it with regulatory interactions. We will use two different Cytoscape apps: [WikiPathways app](https://apps.cytoscape.org/apps/wikipathways) and [CyTargetLinker app](https://cytargetlinker.github.io/). 

In Cytoscape:
* File → Import → Network from Public Databases.
    * Select WikiPathways as a Data Source
    * Search for MECP2 and select only mouse pathways
    * Select the “MECP2 and Associated Rett Syndrome” (WP2910)
* “Import as Pathway”
* Now import again as Network
* Both the pathway and the network view are created

![alt text](img/wikipathways1.PNG "Select pathways from WikiPathways")


> Question 3: Comparing “Pathway” and “Network”
> - List the main differences between the pathway and the network view of the selected process  
> - If you analyze the network (degree/betweenness) what are the similarities and/or differences?

Now you are going to extend the MECP2 and Associated Rett Syndrome (WP2910) pathway visualized as a network using CyTargetLinker with gene-pathway interactions from WikiPathways.
* Go to Apps -> CyTargetLinker -> Exend network
    * Select User Network
        *choose the MECP2 pathway as a network
    * Select your network attribute	
        * choose the name of the column containing a biological identifier (“Ensembl”)
    * Select Link Sets
        * Browse in the UseCase2 files for the directory which contains the LinkSets 
    * Select direction 
        * “BOTH”
•	Click at OK

![alt text](img/cytargetlinker3.PNG "Extend with pathways from WikiPathways")

* Select the mouse WikiPathways analysis collection (wikipathways-mm-20180410.xgmml) LinkSet
* Click at OK
* Open the results panel via Apps -> CyTargetLinker -> Show Results Panel

![alt text](img/cytargetlinker4.PNG "Select pathways from WikiPathways")

> Question 4: Extended pathway with other mouse pathways from WikiPathways
> - How many pathway-gene interactions are added to the network?  
> - Which gene(s) is(are) connected to the highest amount of pathways?

A list of differentially expressed genes (DEGs) in the Purkinje cells located in the cerebellar cortex of the brain of a Mecp2−/y mouse model for Rett syndrome are selected and present in the Data directory of the UseCase2 files. Now we will visualize the logFC and the p-value of the genes in the network.

First you need to add the values, i.e. logFC and p-value, of the DEGs to the network. 

* Go to File -> Import -> Table from File. 
* Select the DEGs.txt in the Data directory of UseCase2. 
* Select the “Ensembl” column as the "Key Column for network"
* Click at Ok

![alt text](img/data1.png "Import DEGs")

Next you will color the nodes according to the logFC. 
* Open the Style Panel and adapt the node color (fill color). 
* Select Map
    *  Column = logFC
    *  Mapping Type = Continuous Mapping. 
* Now the genes of which the expression is significantly downregulated in the Mecp2 mouse model are colored in blue and the upregulated genes in red. 

![alt text](img/data2.png "Data visualization")

* Click at the gradient
* Change the min/max values to -1 and 1

![alt text](img/data3.png "Data visualization min/max")


> Question 5: Gene expression visualized in network
> - Which genes are significantly downregulated?  
> - Which genes are significantly upregulated?




