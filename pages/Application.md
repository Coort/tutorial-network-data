# Combining skills sessions 1, 2 and 3

In the skills sessions you learned, amongst other things, how to obtain centrality measurements in networks, how to create correlation networks, visualize data in networks and extend networks with additional information. In the final assignment of this skills session you should combine the obtained knowledge from these sessions. 

Step 1: Start with selecting a human dataset in the Gene Expression Omnibus (GEO). Choose a topic that is of interest to you, like a specific disease or treatment. 
* Go to [GEO](https://www.ncbi.nlm.nih.gov/geo/), search for a human dataset. 
* Write down the accession number of the dataset starting with GSE.

If you can't find a study yourself you can also use [GSE6955](https://www.ncbi.nlm.nih.gov/sites/GDSbrowser?acc=GDS2613) 


Step 2: To compare two or more groups of Samples in order to identify genes that are differentially expressed across experimental conditions you will use GEO2R
* Go to [GEO2R](https://www.ncbi.nlm.nih.gov/geo/geo2r/) and search with the selected GEO accession number, starting with GSE.
* Define two groups and select a group and thereafter select the samples linked to this group. Below see the example of the GSE6955 dataset that contains brain tissue from normal and rett syndrome patients. 

 
![alt text](img/Geo2R.png "Create Groups in GEO2R")

* Click at "Analyze" to compare the two groups. Within options you can change the settings for the statistical analysis. 
* Click at download full table and you will have your list of genes and for the differentially expressed you could use a cut-off value of the p-value < 0.05 .

Click at R script to see the script used to perform the statistical analysis, but also for the generation of the (QC) plots.


Step 3: Now design and execute an analysis workflow with the list of differentially expressed genes based on the three skills sessions. 

 