# Explore the networks from NDEx 


####Now you will explore a network form the NDEx collection
* Go to File -> Import -> Import network from NDEx.
* Select the "CTD: disease-pathway association"
* Import the network. Please be patient this will take a while since it is a large network.

####Problem: no connection from Cytoscape to the NDEx website
* If you can't get the network via Cytoscape you can also download it from the NDEx website. 
* Go to the NDEx website (https://www.ndexbio.org/) -> search for the network and download it as .cx file. 
* Once you have downloaded the .cx file you Open Cytoscape and go to File -> Import -> Network from File and select the .cx file.

![alt text](img/ndex1.PNG "Select NDEx network")

Since the network is has many nodes and edges we will now only select the nodes linked to the Rett Syndrome nodes. First, search for Rett at the top right, this will select two nodes. 

![alt text](img/ndex2.png "Select Rett syndrome")

You will create a network based on the selected Rett Syndrome nodes and the interactions. 
* Go to Select -> Nodes -> First Neighbors of Selected Nodes -> Undirected. This selects all the nodes linked to the Rett Syndrome nodes. 
* Go to File -> New -> Network -> From select nodes, all edges. A new network is created in Cytoscape
* Change the layout of the created network, Go to Layout -> Apply Preferred Layout.
* Change the style of the network, Go to the "Style" panel at the left hand side and select "Directed"

> Question 6: Subnetwork of a selected NDEx network
> - What is the origin of the select NDEx network (HINT: Explore the Network Table at the bottom of the network)? 
> - What types of interactions are present in the selected network? 








